@extends('layout')
@section('content')
  
<a href="{{ route('telefono.create') }}" class="btn btn-primary m-2">Nueva</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TIPO</th>
      <th scope="col">TELEFONO</th>
      <th scope="col">ELIMINAR</th>
      <th scope="col">ACTUALIZAR</th>
    </tr>
  </thead>
  <tbody>
  @foreach($telefono as $tel)
    <tr>
      <th scope="row">{{$tel->id}}</th>
      <td scope="row">{{$tel->nombre}}</td>
      <td scope="row">{{$tel->direccion}}</td>
      <td scope="row">{{$tel->tipoTel}}</td>
      <td scope="row">{{ $tel->telefono }}</td>
      <td scope="row">
      <form method="POST" action="{{ url("telefono/{$tel->id}") }}" >
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Eliminar</button>
        </form>
      </td>
      <td scope="row">
        <a href="{{ url("telefono/{$tel->id}/edit") }}" class="btn btn-primary m-2">Editar</a>
      </td>
    </tr>
    @endforeach

  </tbody>
</table>


@endsection