@extends('layout')
@section('content')

<div class="container">
<h1>Nuevo</h1>

<form method="POST" action="/telefono">
@csrf   
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre">
  </div>
  <div class="form-group">
    <label for="direccion">Dirección</label>
    <input type="text" class="form-control" id="direccion" name="direccion">
  </div>
  <div class="form-group">
    <label for="direccion">Tipo de telefono</label>
    <input type="text" class="form-control" id="tipoTel" name="tipoTel">
  </div>

  <div class="form-group">
    <label for="direccion">Telefono</label>
    <input type="text" class="form-control" id="telefono" name="telefono">
  </div>
 
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</div>

@endsection