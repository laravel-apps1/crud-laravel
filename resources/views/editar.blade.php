@extends('layout')
@section('content')

<div class="container">
<h1>Editar</h1>
<form method="POST" action="{{ url("telefono/{$telefono->id}") }}">
    @csrf   
    @method('PUT')

  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" value="{{ $telefono->nombre }}" class="form-control" id="nombre" name="nombre">
  </div>
  <div class="form-group">
    <label for="direccion">Dirección</label>
    <input type="text" value="{{ $telefono->direccion }}" class="form-control" id="direccion" name="direccion">
  </div>
  <div class="form-group">
    <label for="direccion">Tipo de telefono</label>
    <input type="text" value="{{ $telefono->tipoTel }}" class="form-control" id="tipoTel" name="tipoTel">
  </div>

  <div class="form-group">
    <label for="direccion">Telefono</label>
    <input type="text" value="{{ $telefono->telefono }}" class="form-control" id="telefono" name="telefono">
  </div>
  <button type="submit" class="btn btn-primary">Editar</button>

</form>
</div>

@endsection