<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class telefono extends Model
{
    protected $table = 'telefonos';


    protected $fillable = [
        'id',
        'nombre',
        'direccion',
        'tipoTel',
        'telefono'
    ];

    public $timestamps = false;
    

}
