<?php

namespace App\Http\Controllers;

use App\telefono;
use Illuminate\Http\Request;

class telefonoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $telefonos = telefono::all();
        return view('consultar')->with('telefono', $telefonos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insertar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $telefono = new telefono;
        $telefono->nombre = $request->nombre;
        $telefono->direccion = $request->direccion;
        $telefono->tipoTel = $request->tipoTel;
        $telefono->telefono = $request->telefono;
        $telefono->save();
        return  redirect('/telefono');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\telefono  $telefono
     * @return \Illuminate\Http\Response
     */
    public function show(telefono $telefono)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\telefono  $telefono
     * @return \Illuminate\Http\Response
     */
    public function edit(telefono $telefono)
    {
        return view('editar')->with('telefono', $telefono);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\telefono  $telefono
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, telefono $telefono)
    {
        $telefono = telefono::where('id',$telefono->id)->firstOrFail();
        $telefono->nombre = $request->nombre;
        $telefono->direccion = $request->direccion;
        $telefono->tipoTel = $request->tipoTel;
        $telefono->telefono = $request->telefono;
        $telefono->save();
        return  redirect('/telefono');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\telefono  $telefono
     * @return \Illuminate\Http\Response
     */
    public function destroy(telefono $telefono)
    {
        $telefono->delete();
        return  redirect('/telefono');    
    }
}
